const list =[
  {"id":0,"apellido":" MURILLO LAJE","nombre":" NAUN POLICARPO ","semestre":"5to","paralelo":"A",
  "direccion":"av1calle01?","telefono":"0989676566","correo":"01@gmail.com"}
  ,{"id":1,"apellido":" BRIONES GUTIERREZ","nombre":" DIXON ELIAN  ","semestre":"5to","paralelo":"A"
  ,"direccion":"av2calle02","telefono":"0989676565","correo":"02@gmail.com"}
  ,{"id":2,"apellido":" CHAVEZ PAZ","nombre":" JOSE BAUTISTA   ","semestre":["5to"],"paralelo":"A"
  ,"direccion":"av3calle03","telefono":"0989343455","correo":"03@gmail.com"}
  ,{"id":3,"apellido":" MACIAS MERA","nombre":" NATALIA ANAHI ","semestre":"6to","paralelo":"B"
  ,"direccion":"av4calle04","telefono":"0989344455","correo":"04@gmail.com"}
  ,{"id":4,"apellido":" MARRASQUIN MOREIRA","nombre":" ERICK JOAO ","semestre":"6to","paralelo":"B"
  ,"direccion":"av5calle05","telefono":"0982323445","correo":"05@gmail.com"}
  ,{"id":5,"apellido":" ANCHUNDIA DELGADO","nombre":" LENIN MOISES ","semestre":"6to","paralelo":"B"
  ,"direccion":"av6calle06","telefono":"0983234874","correo":"06@gmail.com"}
  ,{"id":6,"apellido":" MENDOZA ANCHUNDIA","nombre":" JAROD DENNIS ","semestre":"6to","paralelo":"B"
  ,"direccion":"av7calle07","telefono":"0982325456","correo":"07@gmail.com"}
  ,{"id":7,"apellido":" LIMON YAGUAL","nombre":" OMAR LEONARDO ","semestre":"6to","paralelo":"B"
  ,"direccion":"av8calle08","telefono":"0938988564","correo":"08@gmail.com"}
  ,{"id":8,"apellido":" ARAUZ CHICHANDA","nombre":" JAVIER ARCENIO ","semestre":"5to","paralelo":"B"
  ,"direccion":"av9calle09","telefono":"0982345564","correo":"09@gmail.com"}
  ,{"id":9,"apellido":" VERA MERO","nombre":" BORYS HERNAN ","semestre":"5to","paralelo":"B",
  "direccion":"av10calle110","telefono":"0982322456","correo":"10@gmail.com"}]


const estudiantes = document.querySelectorAll('.nombres');

  estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id=nombre.target.getAttribute('ide');
        list.forEach((estudiante)=>{
            if(id == estudiante.id){
                const verDetalle=nombre.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                        <div class="lista">
                                        <div class="estu">
                                        <h2>Apellido:</h2>
                                        <p>${estudiante.apellido}</p>
                                        </div>

                                        <div class="estu">
                                        <h2>Nombre:</h2>
                                        <p>${estudiante.nombre}</p>
                                        </div>

                                        <div class="estu">
                                            <h2>Semestre:</h2>
                                            <p>${estudiante.semestre}</p>
                                        </div>
                                        <div class="estu">
                                            <h2>Paralelo:</h2>
                                            <p>${estudiante.paralelo}</p>
                                        </div>
                                        <div class="estu">
                                            <h2>Direccion:</h2>
                                            <p>${estudiante.direccion}</p>
                                        </div>
                                        <div class="estu">
                                            <h2>Telefono:</h2>
                                            <p>${estudiante.telefono}</p>
                                        </div>
                                        <div class="estu">
                                            <h2>Correo:</h2>
                                            <p>${estudiante.correo}</p>
                                        </div> 
                                    </div>`

            }
        })
    })
})
